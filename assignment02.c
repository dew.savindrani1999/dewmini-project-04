#include <stdio.h>
int main() {
  int z, i, flag = 0;
  printf("Enter a number ");
  scanf("%d", &z);

  for (i = 2; i<z ; ++i) {
    if (z % i == 0) {
      flag = 1;
      break;
    }
  }

  if (z == 1) {
    printf("1 is not a prime number or a composite number");
  }
  else {
    if (flag == 1)
      printf("%d is not a prime number.", z);
    else
      printf("%d is a prime number.", z);
  }

  return 0;
}




